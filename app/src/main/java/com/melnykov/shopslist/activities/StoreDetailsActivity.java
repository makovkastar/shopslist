package com.melnykov.shopslist.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.melnykov.shopslist.R;
import com.melnykov.shopslist.fragments.StoreDetailsFragment;

public class StoreDetailsActivity extends ActionBarActivity {

    public static final String EXTRA_STORE_ID = "com.melnykov.shopslist.EXTRA_STORE_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_details);

        if (savedInstanceState == null) {
            long storeId = getIntent().getLongExtra(EXTRA_STORE_ID, 0);

            getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, StoreDetailsFragment.newInstance(storeId))
                .commit();
        }
    }
}