package com.melnykov.shopslist.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.melnykov.shopslist.R;

public class StoreListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_list);
    }
}