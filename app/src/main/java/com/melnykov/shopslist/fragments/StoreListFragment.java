package com.melnykov.shopslist.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;

import com.melnykov.shopslist.R;
import com.melnykov.shopslist.activities.StoreDetailsActivity;
import com.melnykov.shopslist.async.FetchDataAsyncTask;
import com.melnykov.shopslist.db.DbHelper;
import com.melnykov.shopslist.utils.SimpleCursorLoader;

public class StoreListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_ID_INSTRUMENTS = 0;

    private SimpleCursorAdapter mStoreAdapter;

    private boolean mDualPane;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] from = new String[]{DbHelper.StoreEntry.COLUMN_NAME, DbHelper.StoreEntry.COLUMN_ADDRESS};
        int to[] = new int[]{android.R.id.text1, android.R.id.text2};
        mStoreAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_2, null,
            from, to, 0);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = mStoreAdapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    showDetails(cursor.getInt(cursor.getColumnIndex(DbHelper.StoreEntry.COLUMN_ID)));
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListAdapter(mStoreAdapter);

        View detailsFrame = getActivity().findViewById(R.id.store_details);
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;
        // We may use a mDualPane to check later if we should show master/detail flow layout.

        getLoaderManager().initLoader(LOADER_ID_INSTRUMENTS, null, this);

        new FetchDataAsyncTask(new FetchDataAsyncTask.Callback() {
            @Override
            public void onSuccess() {
                getLoaderManager().getLoader(LOADER_ID_INSTRUMENTS).forceLoad();
            }
        }).execute();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new SimpleCursorLoader(getActivity()) {
            @Override
            public Cursor loadInBackground() {
                SQLiteDatabase db = DbHelper.getInstance(getContext()).getReadableDatabase();
                return db.query(DbHelper.StoreEntry.TABLE_NAME,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mStoreAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mStoreAdapter.swapCursor(null);
    }

    private void showDetails(long storeId) {
        Intent intent = new Intent(getActivity(), StoreDetailsActivity.class);
        intent.putExtra(StoreDetailsActivity.EXTRA_STORE_ID, storeId);
        startActivity(intent);
    }
}