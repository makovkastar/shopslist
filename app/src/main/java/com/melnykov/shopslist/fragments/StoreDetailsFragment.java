package com.melnykov.shopslist.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.melnykov.shopslist.R;
import com.melnykov.shopslist.activities.StoreDetailsActivity;
import com.melnykov.shopslist.db.DbHelper;
import com.melnykov.shopslist.utils.SimpleCursorLoader;

public class StoreDetailsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_ID_STORE_DETAILS = 0;
    private static final int LOADER_ID_INSTRUMENTS = 1;

    private TextView mNameText;
    private TextView mAddressText;
    private TextView mPhoneText;
    private TextView mWebsiteText;

    private TextView mTotalInstrumentsText;

    private SimpleCursorAdapter mInstrumentAdapter;

    public static StoreDetailsFragment newInstance(long storeId) {
        StoreDetailsFragment fragment = new StoreDetailsFragment();
        Bundle args = new Bundle();
        args.putLong(StoreDetailsActivity.EXTRA_STORE_ID, storeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] from = new String[]{DbHelper.InstrumentStockEntry.COLUMN_BRAND,
            DbHelper.InstrumentStockEntry.COLUMN_MODEL, DbHelper.InstrumentStockEntry.COLUMN_TYPE,
            DbHelper.InstrumentStockEntry.COLUMN_PRICE};
        int to[] = new int[]{R.id.brand, R.id.model, R.id.type, R.id.price};
        mInstrumentAdapter = new SimpleCursorAdapter(getActivity(), R.layout.instrument_list_item, null,
            from, to, 0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(LOADER_ID_STORE_DETAILS, null, this);
        getLoaderManager().initLoader(LOADER_ID_INSTRUMENTS, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_store_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNameText = (TextView) view.findViewById(R.id.store_name);
        mAddressText = (TextView) view.findViewById(R.id.store_address);
        mPhoneText = (TextView) view.findViewById(R.id.store_phone);
        mWebsiteText = (TextView) view.findViewById(R.id.store_website);

        mTotalInstrumentsText = (TextView) view.findViewById(R.id.total_instruments);

        ListView instrumentList = (ListView) view.findViewById(R.id.instrument_list);
        instrumentList.setAdapter(mInstrumentAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, Bundle args) {
        return new SimpleCursorLoader(getActivity()) {

            @Override
            public Cursor loadInBackground() {
                long storeId = getArguments().getLong(StoreDetailsActivity.EXTRA_STORE_ID);
                SQLiteDatabase db = DbHelper.getInstance(getContext()).getReadableDatabase();
                switch (id) {
                    case LOADER_ID_STORE_DETAILS:
                        return db.query(DbHelper.StoreEntry.TABLE_NAME,
                            null,
                            DbHelper.StoreEntry.COLUMN_ID + " = ?",
                            new String[]{Long.toString(storeId)},
                            null,
                            null,
                            null);
                    case LOADER_ID_INSTRUMENTS:
                        return db.query(DbHelper.InstrumentStockEntry.TABLE_NAME,
                            null,
                            DbHelper.InstrumentStockEntry.COLUMN_STORE_ID + " = ?",
                            new String[]{Long.toString(storeId)},
                            null,
                            null,
                            null);
                    default:
                        throw new IllegalArgumentException("Unknown loader id: " + id);
                }
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_ID_STORE_DETAILS:
                if (cursor != null && cursor.moveToFirst()) {
                    mNameText.setText(cursor.getString(cursor.getColumnIndex(DbHelper.StoreEntry.COLUMN_NAME)));
                    mAddressText.setText(cursor.getString(cursor.getColumnIndex(DbHelper.StoreEntry.COLUMN_ADDRESS)));
                    mPhoneText.setText(cursor.getString(cursor.getColumnIndex(DbHelper.StoreEntry.COLUMN_PHONE)));
                    mWebsiteText.setText(cursor.getString(cursor.getColumnIndex(DbHelper.StoreEntry.COLUMN_WEBSITE)));
                }
                break;
            case LOADER_ID_INSTRUMENTS:
                mInstrumentAdapter.swapCursor(cursor);
                int totalInstruments = 0;
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        totalInstruments += cursor.getInt(
                            cursor.getColumnIndex(DbHelper.InstrumentStockEntry.COLUMN_QUANTITY));
                    }
                }
                mTotalInstrumentsText.setText("Total instruments in stock: " + totalInstruments);
                break;
            default:
                // NOP
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LOADER_ID_INSTRUMENTS:
                mInstrumentAdapter.swapCursor(null);
                break;
            default:
                // NOP
        }
    }
}