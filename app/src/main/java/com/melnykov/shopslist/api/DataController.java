package com.melnykov.shopslist.api;

import com.melnykov.shopslist.model.InstrumentStock;
import com.melnykov.shopslist.model.Store;

import java.util.List;

import retrofit.RestAdapter;

public class DataController {

    private static DataController sInstance;

    private final ApiService mService;

    private DataController() {
        mService = createApiService();
    }

    public static synchronized DataController getInstance() {
        if (sInstance == null) {
            sInstance = new DataController();
        }
        return sInstance;
    }

    private ApiService createApiService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://aschoolapi.appspot.com/")
            .build();

        return restAdapter.create(ApiService.class);
    }

    public List<Store> getStores() {
        return mService.getStores();
    }

    public List<InstrumentStock> getInstrumentStocks(long storeId) {
        return mService.getInstrumentStocks(storeId);
    }
}