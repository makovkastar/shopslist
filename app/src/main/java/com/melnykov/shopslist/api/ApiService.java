package com.melnykov.shopslist.api;

import com.melnykov.shopslist.model.InstrumentStock;
import com.melnykov.shopslist.model.Store;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

public interface ApiService {

    @GET("/stores")
    List<Store> getStores();

    @GET("/stores/{shop_id}/instruments")
    List<InstrumentStock> getInstrumentStocks(@Path("shop_id") long shopId);
}