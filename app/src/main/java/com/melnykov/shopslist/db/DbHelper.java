package com.melnykov.shopslist.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.melnykov.shopslist.model.Instrument;
import com.melnykov.shopslist.model.InstrumentStock;
import com.melnykov.shopslist.model.Store;

public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "shops_list.db";
    private static final int DB_VERSION = 1;

    private static DbHelper sInstance;

    private DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized DbHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DbHelper(context);
        }
        return sInstance;
    }

    public static ContentValues storeToContentValues(Store store) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StoreEntry.COLUMN_ID, store.getId());
        contentValues.put(StoreEntry.COLUMN_NAME, store.getName());
        contentValues.put(StoreEntry.COLUMN_ADDRESS, store.getAddress());
        contentValues.put(StoreEntry.COLUMN_PHONE, store.getPhone());
        contentValues.put(StoreEntry.COLUMN_WEBSITE, store.getWebsite());
        if (store.getLocation() != null) {
            contentValues.put(StoreEntry.COLUMN_LATITUDE, store.getLocation().getLatitude());
            contentValues.put(StoreEntry.COLUMN_LONGITUDE, store.getLocation().getLongitude());
        }
        return contentValues;
    }

    public static ContentValues instrumentStockToContentValues(InstrumentStock instrumentStock) {
        ContentValues contentValues = new ContentValues();
        Instrument instrument = instrumentStock.getInstrument();
        if (instrument != null) {
            contentValues.put(InstrumentStockEntry.COLUMN_ID, instrument.getId());
            contentValues.put(InstrumentStockEntry.COLUMN_BRAND, instrument.getBrand());
            contentValues.put(InstrumentStockEntry.COLUMN_MODEL, instrument.getModel());
            contentValues.put(InstrumentStockEntry.COLUMN_TYPE, instrument.getType());
            contentValues.put(InstrumentStockEntry.COLUMN_PRICE, instrument.getPrice());
        }
        contentValues.put(InstrumentStockEntry.COLUMN_QUANTITY, instrumentStock.getQuantity());
        contentValues.put(InstrumentStockEntry.COLUMN_STORE_ID, instrumentStock.getStoreId());
        return contentValues;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_STORE_TABLE = "CREATE TABLE " + StoreEntry.TABLE_NAME + " (" +
            StoreEntry._ID + " INTEGER PRIMARY KEY, " +
            StoreEntry.COLUMN_ID + " INTEGER NOT NULL, " +
            StoreEntry.COLUMN_NAME + " TEXT, " +
            StoreEntry.COLUMN_ADDRESS + " TEXT, " +
            StoreEntry.COLUMN_PHONE + " TEXT, " +
            StoreEntry.COLUMN_WEBSITE + " TEXT, " +
            StoreEntry.COLUMN_LATITUDE + " INTEGER, " +
            StoreEntry.COLUMN_LONGITUDE + " INTEGER, " +

            // To assure the application have just one store entry with same id,
            // it's created a UNIQUE constraint with REPLACE strategy
            "UNIQUE (" + StoreEntry.COLUMN_ID + ") ON CONFLICT REPLACE);";

        final String SQL_CREATE_INSTRUMENT_STOCK_TABLE = "CREATE TABLE " + InstrumentStockEntry.TABLE_NAME + " (" +
            InstrumentStockEntry._ID + " INTEGER PRIMARY KEY, " +
            InstrumentStockEntry.COLUMN_ID + " INTEGER NOT NULL, " +
            InstrumentStockEntry.COLUMN_BRAND + " TEXT, " +
            InstrumentStockEntry.COLUMN_MODEL + " TEXT, " +
            InstrumentStockEntry.COLUMN_TYPE + " TEXT, " +
            InstrumentStockEntry.COLUMN_PRICE + " REAL, " +
            InstrumentStockEntry.COLUMN_QUANTITY + " INTEGER, " +
            InstrumentStockEntry.COLUMN_STORE_ID + " INTEGER, " +

            // To assure the application have just one instrument stock entry with same store id and own id,
            // it's created a UNIQUE constraint with REPLACE strategy
            "UNIQUE (" + InstrumentStockEntry.COLUMN_ID + "," + InstrumentStockEntry.COLUMN_STORE_ID + ") ON CONFLICT REPLACE);";

        db.execSQL(SQL_CREATE_STORE_TABLE);
        db.execSQL(SQL_CREATE_INSTRUMENT_STOCK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // For now just simply drop and recreate the table
        db.execSQL("DROP TABLE IF EXISTS " + StoreEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + InstrumentStockEntry.TABLE_NAME);
        onCreate(db);
    }

    public static final class StoreEntry implements BaseColumns {

        public static final String TABLE_NAME = "store";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_ADDRESS = "address";
        public static final String COLUMN_PHONE = "phone";
        public static final String COLUMN_WEBSITE = "website";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";

    }

    public static final class InstrumentStockEntry implements BaseColumns {

        public static final String TABLE_NAME = "instrument_stock";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_BRAND = "brand";
        public static final String COLUMN_MODEL = "model";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_QUANTITY = "quantity";
        public static final String COLUMN_STORE_ID = "store_id";
    }
}