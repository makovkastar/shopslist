package com.melnykov.shopslist;

import android.app.Application;
import android.content.Context;

public class ShopsListApplication extends Application {

    private static ShopsListApplication sInstance;

    public static Context getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }
}