package com.melnykov.shopslist.model;

public class InstrumentStock {

    private Instrument instrument;
    private int quantity;
    private long storeId;

    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getStoreId() {
        return storeId;
    }

    public void setStoreId(long storeId) {
        this.storeId = storeId;
    }

    @Override
    public String toString() {
        return "InstrumentStock{" +
            "instrument=" + instrument +
            ", quantity=" + quantity +
            ", storeId=" + storeId +
            '}';
    }
}