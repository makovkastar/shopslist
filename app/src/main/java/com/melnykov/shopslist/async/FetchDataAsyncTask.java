package com.melnykov.shopslist.async;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.melnykov.shopslist.ShopsListApplication;
import com.melnykov.shopslist.api.DataController;
import com.melnykov.shopslist.db.DbHelper;
import com.melnykov.shopslist.model.InstrumentStock;
import com.melnykov.shopslist.model.Store;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

// We also may use a Service instead of AsyncTask and then notify UI with a broadcast or an event using event bus.
public class FetchDataAsyncTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = FetchDataAsyncTask.class.getSimpleName();

    // We store a reference to a callback as a WeakReference to avoid leaking a Context.
    private final WeakReference<Callback> mCallback;

    public FetchDataAsyncTask(Callback callback) {
        mCallback = new WeakReference<>(callback);
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d(TAG, "Start fetching data");

        Context appContext = ShopsListApplication.getInstance().getApplicationContext();

        DataController controller = DataController.getInstance();
        SQLiteDatabase db = DbHelper.getInstance(appContext).getWritableDatabase();

        // Get all stores from a server
        List<Store> stores = controller.getStores();
        List<InstrumentStock> instrumentStocks = new ArrayList<>();

        // Get all instrument stocks from these stores. We use a "big cookie model" (load as much as possible
        // data at once to avoid battery draining.

        // In case of a project on production data fetching model should be balanced between amount of
        // data being loaded and number of data requests to load all required data.

        // For example if a list of stores is huge we may load all store items but load details only for the
        // first 20 items. And then when a user clicks on a particular item load details for it.
        for (Store store : stores) {
            List<InstrumentStock> _instrumentStocks = controller.getInstrumentStocks(store.getId());
            // Assign a store id for each item
            for (InstrumentStock instrumentStock : _instrumentStocks) {
                instrumentStock.setStoreId(store.getId());
            }
            instrumentStocks.addAll(_instrumentStocks);
        }

        // Save all stores to the database in one transaction.
        db.beginTransaction();
        try {
            for (InstrumentStock instrumentStock : instrumentStocks) {
                db.insert(DbHelper.InstrumentStockEntry.TABLE_NAME, null,
                    DbHelper.instrumentStockToContentValues(instrumentStock));
            }

            for (Store store : stores) {
                db.insert(DbHelper.StoreEntry.TABLE_NAME, null, DbHelper.storeToContentValues(store));
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Callback callback = mCallback.get();
        if (callback != null) {
            callback.onSuccess();
        }
    }

    public interface Callback {
        void onSuccess();
    }
}